package test;

import static org.junit.Assert.*;

import java.util.List;

import org.junit.Before;
import org.junit.Test;

import com.retail.core.EmployeeListClass;
import com.retail.core.Employees;

public class EmployeeClassJunit {

	Employees e;
	EmployeeListClass l;

	@Before
	public void doSetup() {
		e = new Employees();
		l = new EmployeeListClass();
	}

	@Test
	public void testListSize() {

		List<Employees> list = l.getAllEmployees();
		int actual = list.size();
		int expected = 4;

		assertEquals(expected, actual);

		System.out.println(actual);
	}

	@Test
	public void testNamesByWorkingHours() {
		List<Employees> listing = l.getAllEmployees();

		String actual = null;
		String expected = "Vishal";
		for (Employees ee : listing) {
			if (ee.getHoursWorked() < 40) {
				actual = ee.getFirstName();
				System.out.println(actual);
			}
		}
		assertEquals(expected, actual);
	}
	
	@Test
	public void testUniqueIdByName() {
		List<Employees> listing = l.getAllEmployees();
		int actual = 0;
		double expected = 101;
		for (Employees ee : listing) {
			if (ee.getFirstName().equals("Justin")) {
				actual = ee.getId();
				System.out.println(actual);
			}
		}
		assertEquals(expected, actual,.001);
	}
}
