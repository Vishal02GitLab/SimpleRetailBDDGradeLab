package test;

import static org.junit.Assert.*;

import java.io.File;

import org.apache.commons.io.FileUtils;
import org.junit.Test;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class JenkinsScreenShot {

	@Test
	public void test() throws InterruptedException {
		
		String os = System.getProperty("os.name").toLowerCase().split(" ")[0];
		
		if("mac".contains(os)) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "/chromedriver");
			
			
		}else {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
		}
		WebDriver driver = new ChromeDriver();
		try {
		driver.get("http://localhost:8081");
		driver.manage().window().maximize();
		
		WebElement enterId = driver.findElement(By.name("j_username"));
		enterId.sendKeys("Vishal02");
		
		WebElement enterPass = driver.findElement(By.name("j_password"));
		enterPass.sendKeys("Vishal8!");
		driver.findElement(By.id("yui-gen1-button")).click();
		
		if(driver instanceof TakesScreenshot) {
			TakesScreenshot screenShorter = (TakesScreenshot) driver;
			File outputFile = screenShorter.getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(outputFile, new File("c:\\\\vishal\\\\screenshot.png"));
			
		driver.findElement(By.xpath("//*[@id=\"header\"]/div[2]/span/a[2]/b")).click();
		

		 Thread.sleep(6000);
			
		}
		 Thread.sleep(5000);
		}catch(Exception e) {
			e.printStackTrace();
		}finally {
			driver.close();
			driver.quit();
			
		}
	}

}
