package test;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

import cucumber.api.DataTable;
import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class PriceConfirmStepDefinition {

	Map<String, Double> upcList = new HashMap<String, Double>();
	
	WebDriver driver;

	@Before("@Web")
	public void setUp() {

String os = System.getProperty("os.name").toLowerCase().split(" ")[0];
		
		if("mac".contains(os)) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "/chromedriver");
			
			
		}else {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
		}
		 driver = new ChromeDriver();
	}
	
	 @After
	    public void tearDown(){
		 driver.close();
		 driver.quit();
	    }
	
	@Given("^the following Data:$")
	public void the_following_Data(DataTable upcPrices) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    // For automatic transformation, change DataTable to one of
	    // List<YourType>, List<List<E>>, List<Map<K,V>> or Map<K,V>.
	    // E,K,V must be a scalar (String, Integer, Date, enum etc)
	    //throw new PendingException();
		
	
		upcList = upcPrices.asMap(String.class, Double.class);
		
		System.out.println("========UPC list from feature=======");
		for (String key : upcList.keySet()) {
			System.out.println("UPC: " + key + "PRICES: " + upcList.get(key));
		}
		System.out.println(upcList.size());
	}

	@When("^User Navigates to create Shipment Page$")
	public void user_Navigates_to_create_Shipment_Page() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	  
		driver.get("http://localhost:8080/SimpleRetailWeb_starter/index.html");
		
		Thread.sleep(2000);
		String xpathVal = "//input[@value='Get Items To Be Shipped']";
		WebElement button = driver.findElement(By.xpath(xpathVal));
		
		///html/body/form/input
		button.click();
		Thread.sleep(3000);
	}

	@Then("^price from the page should match the given upc$")
	public void price_from_the_page_should_match_the_given_upc() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		
		List<WebElement> tdUpc = driver.findElements(By.xpath("//tbody/tr/td[2]"));
		List<WebElement> tdPrice = driver.findElements(By.xpath("//tbody/tr/td[4]"));
		
		Map<String, Double> upcPriceMap = new HashMap<String, Double>();
		
		for(int i=0 ; i<tdUpc.size(); i++) {
			upcPriceMap.put(tdUpc.get(i).getText(), Double.parseDouble(tdPrice.get(i).getText()));
		}
		System.out.println(tdUpc.size());
		
		for(int j=0 ; j<tdUpc.size(); j++) {
			Double expected = upcList.get(tdUpc.get(j).getText());
			//assertEquals(expected, Double.parseDouble(tdPrice.get(j).getText()),.001);
			 assertEquals(expected, Double.parseDouble(tdPrice.get(j).getText()),.001);
			 // upcList.get(Double.parseDouble(tdUpc.get(j).getText()))
		}
	}
}
