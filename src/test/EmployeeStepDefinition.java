package test;

import java.sql.Driver;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

import com.retail.core.Employees;

import cucumber.api.java.After;
import cucumber.api.java.Before;
import cucumber.api.java.en.Given;
import cucumber.api.java.en.Then;
import cucumber.api.java.en.When;

public class EmployeeStepDefinition {
	
	WebDriver driver;
	Employees employees = new Employees();;
	
	@Before("@Web")
	public void setUp() {

String os = System.getProperty("os.name").toLowerCase().split(" ")[0];
		
		if("mac".contains(os)) {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "/chromedriver");
			
			
		}else {
			System.setProperty("webdriver.chrome.driver", System.getProperty("user.dir")+ "\\chromedriver.exe");
		}
		 driver = new ChromeDriver();
	}
	
	
	@Given("^I have a \"([^\"]*)\" of Employee in company$")
	public void i_have_a_of_Employee_in_company(String name) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
		
		employees.setFirstName(name);
		
		
	}

	@Given("^It only show in records as i work (\\d+) hours$")
	public void it_only_show_in_records_as_i_work_hours(int hours) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		employees.setHoursWorked(hours);
	}

	@When("^I select name from list & click button$")
	public void i_select_name_from_list_click_button() throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	    //throw new PendingException();
		driver = new ChromeDriver();
       driver.get("http://localhost:8080/SimpleRetailWeb_starter/index.html");
		
		Thread.sleep(2000);
		String xpathVal = "//input[@value='Get Items To Be Shipped']";
		WebElement button = driver.findElement(By.xpath(xpathVal));
		
		button.click();
		Thread.sleep(2000);
		
		List<WebElement> checkElements = driver.findElements(By.xpath("//*[@id=\"checkedRows\"]"));
		checkElements.get(0).click();
		checkElements.get(1).click();
		
		WebElement selectdd = driver.findElement(By.id("selectCategory"));
		Select selelctElement = new Select (selectdd);
		selelctElement.selectByVisibleText("Vishal");

	
		
		String xpath ="//input[@value='Create Shipment']";
		WebElement b = driver.findElement(By.xpath(xpath));
		b.click();
		Thread.sleep(3000);
		
	}

	@Then("^user navigates to next page & it shows my id (\\d+)$")
	public void user_navigates_to_next_page_it_shows_my_id(int arg1) throws Throwable {
	    // Write code here that turns the phrase above into concrete actions
	   // throw new PendingException();
	}
	
	 @After
	    public void tearDown(){
		 driver.close();
		 driver.quit();
	    }
}
