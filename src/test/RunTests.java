package test;

import static org.junit.Assert.*;

import org.junit.runner.RunWith;

import cucumber.api.CucumberOptions;
import cucumber.api.junit.Cucumber;

@RunWith(Cucumber.class)
@CucumberOptions(plugin = {"pretty",
		"html:target/Destination"},
features = "src/test/specs",
monochrome = true
		)
public class RunTests {

	

}
