Feature: Prices are valid on shipment.jsp page
Ensure proper price are on shipment.jsp for each String upc code

@Web
Scenario: UPCS
	Given the following Data:
	|567321101987  | 19.99 |
	|567321101986  | 17.99 |
	|567321101985  | 20.49 |
	|567321101984  |23.88  |
	|467321101899  |9.75   |
	|477321101878  |17.25  |
	
	When User Navigates to create Shipment Page
	
	Then price from the page should match the given upc