package com.retail.web;

import java.io.IOException;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.retail.core.EmployeeListClass;
import com.retail.core.Employees;
import com.retail.core.Item;
import com.retail.core.Products;

/**
 * Servlet implementation class ShippingServlet
 */
@WebServlet("/ShippingServlet")
public class ShippingServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
	List<Employees> myEmp = EmployeeListClass.getAllEmployees();
	Employees ee = new Employees();
	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		List<Item> myItems = Products.getItems();

		request.getSession().setAttribute("items", myItems);
		
		
		request.getSession().setAttribute("employee", myEmp);
		

		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shipment.jsp");
		dispatcher.forward(request, response);
	}

	/**
	 * @see HttpServlet#doPost(HttpServletRequest request, HttpServletResponse
	 *      response)
	 */
	@SuppressWarnings("unchecked")
	protected void doPost(HttpServletRequest request, HttpServletResponse response)
			throws ServletException, IOException {
		
		String[] checkedIds = request.getParameterValues("checkedRows");
		
		String employee = request.getParameter("employee");
        System.out.println(employee);

		List<Item> shipItems = (List<Item>) request.getSession().getAttribute("items");
		
		for (Iterator<Item> iter = (Iterator<Item>)shipItems.listIterator(); 
				iter.hasNext();
				)
		{
			if (!(Arrays.asList(checkedIds).contains(iter.next().getUpc()))) {
				iter.remove();
			}

		}

		request.getSession().setAttribute("shipItems", shipItems);
        request.getSession().setAttribute("id", employee);
        
	//	if("emp.firstName".equalsIgnoreCase("selectCategory"))
		//int id = request.getParameter(arg0)
			// ee.setId(request.getParameter("emp.id"));
		
		//request.getSession().setAttribute("employees", myEmp);
		
		
		
		RequestDispatcher dispatcher = getServletContext().getRequestDispatcher("/shipConfirmation.jsp");
		dispatcher.forward(request, response);
	}

}
