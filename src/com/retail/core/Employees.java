package com.retail.core;

import java.io.Serializable;

public class Employees implements Serializable{

	private int id;
	private String firstName;
	private String lastName;
	private double hoursWorked;
	
	public Employees(){}
	
	public Employees(int id, String firstName,String lastName, double hoursWorked){
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.hoursWorked = hoursWorked;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}

	public String getLastName() {
		return lastName;
	}

	public void setLastName(String lastName) {
		this.lastName = lastName;
	}

	public double getHoursWorked() {
		return hoursWorked;
	}

	public void setHoursWorked(double hoursWorked) {
		this.hoursWorked = hoursWorked;
	}
	
	
}
