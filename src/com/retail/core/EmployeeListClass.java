package com.retail.core;

import java.util.ArrayList;
import java.util.List;

public class EmployeeListClass {

	Employees e = new Employees();
	
	public static List<Employees> getAllEmployees(){
		
		List<Employees> empList = new ArrayList<Employees>();
		
		empList.add(new Employees(101,"Justin","Poole", 35));
		empList.add(new Employees(102,"Warren","Buffet", 41));
		empList.add(new Employees(103,"Alon","Musk", 41));
		empList.add(new Employees(104,"Vishal","Agarwal", 39));
		
		return empList;
		
	}
	
	public static void getEmployeeId() {
		
	}
}
