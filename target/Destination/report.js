$(document).ready(function() {var formatter = new CucumberHTML.DOMFormatter($('.cucumber-report'));formatter.uri("empName.feature");
formatter.feature({
  "line": 1,
  "name": "Select EmployeeName",
  "description": "",
  "id": "select-employeename",
  "keyword": "Feature"
});
formatter.before({
  "duration": 5423432634,
  "status": "passed"
});
formatter.before({
  "duration": 4198205728,
  "status": "passed"
});
formatter.scenario({
  "line": 4,
  "name": "Select name \u0026 Get id",
  "description": "",
  "id": "select-employeename;select-name-\u0026-get-id",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 3,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 5,
  "name": "I have a \"Justin\" of Employee in company",
  "keyword": "Given "
});
formatter.step({
  "line": 6,
  "name": "It only show in records as i work 35 hours",
  "keyword": "And "
});
formatter.step({
  "line": 7,
  "name": "I select name from list \u0026 click button",
  "keyword": "When "
});
formatter.step({
  "line": 8,
  "name": "user navigates to next page \u0026 it shows my id 101",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Justin",
      "offset": 10
    }
  ],
  "location": "EmployeeStepDefinition.i_have_a_of_Employee_in_company(String)"
});
formatter.result({
  "duration": 171753516,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "35",
      "offset": 34
    }
  ],
  "location": "EmployeeStepDefinition.it_only_show_in_records_as_i_work_hours(int)"
});
formatter.result({
  "duration": 917039,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepDefinition.i_select_name_from_list_click_button()"
});
formatter.result({
  "duration": 12930131478,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "101",
      "offset": 45
    }
  ],
  "location": "EmployeeStepDefinition.user_navigates_to_next_page_it_shows_my_id(int)"
});
formatter.result({
  "duration": 95051,
  "status": "passed"
});
formatter.after({
  "duration": 903629286,
  "status": "passed"
});
formatter.after({
  "duration": 706239298,
  "status": "passed"
});
formatter.before({
  "duration": 4150816031,
  "status": "passed"
});
formatter.before({
  "duration": 4011455172,
  "status": "passed"
});
formatter.scenario({
  "line": 11,
  "name": "Select employeeName \u0026 Get id",
  "description": "",
  "id": "select-employeename;select-employeename-\u0026-get-id",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 10,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 12,
  "name": "I have a \"Vishal\" of Employee in company",
  "keyword": "Given "
});
formatter.step({
  "line": 13,
  "name": "It only show in records as i work 39 hours",
  "keyword": "And "
});
formatter.step({
  "line": 14,
  "name": "I select name from list \u0026 click button",
  "keyword": "When "
});
formatter.step({
  "line": 15,
  "name": "user navigates to next page \u0026 it shows my id 104",
  "keyword": "Then "
});
formatter.match({
  "arguments": [
    {
      "val": "Vishal",
      "offset": 10
    }
  ],
  "location": "EmployeeStepDefinition.i_have_a_of_Employee_in_company(String)"
});
formatter.result({
  "duration": 74077,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "39",
      "offset": 34
    }
  ],
  "location": "EmployeeStepDefinition.it_only_show_in_records_as_i_work_hours(int)"
});
formatter.result({
  "duration": 85680,
  "status": "passed"
});
formatter.match({
  "location": "EmployeeStepDefinition.i_select_name_from_list_click_button()"
});
formatter.result({
  "duration": 12815610540,
  "status": "passed"
});
formatter.match({
  "arguments": [
    {
      "val": "104",
      "offset": 45
    }
  ],
  "location": "EmployeeStepDefinition.user_navigates_to_next_page_it_shows_my_id(int)"
});
formatter.result({
  "duration": 218662,
  "status": "passed"
});
formatter.after({
  "duration": 791662669,
  "status": "passed"
});
formatter.after({
  "duration": 811564414,
  "status": "passed"
});
formatter.uri("price.feature");
formatter.feature({
  "line": 1,
  "name": "Prices are valid on shipment.jsp page",
  "description": "Ensure proper price are on shipment.jsp for each String upc code",
  "id": "prices-are-valid-on-shipment.jsp-page",
  "keyword": "Feature"
});
formatter.before({
  "duration": 4348628615,
  "status": "passed"
});
formatter.before({
  "duration": 4292958795,
  "status": "passed"
});
formatter.scenario({
  "line": 5,
  "name": "UPCS",
  "description": "",
  "id": "prices-are-valid-on-shipment.jsp-page;upcs",
  "type": "scenario",
  "keyword": "Scenario",
  "tags": [
    {
      "line": 4,
      "name": "@Web"
    }
  ]
});
formatter.step({
  "line": 6,
  "name": "the following Data:",
  "rows": [
    {
      "cells": [
        "567321101987",
        "19.99"
      ],
      "line": 7
    },
    {
      "cells": [
        "567321101986",
        "17.99"
      ],
      "line": 8
    },
    {
      "cells": [
        "567321101985",
        "20.49"
      ],
      "line": 9
    },
    {
      "cells": [
        "567321101984",
        "23.88"
      ],
      "line": 10
    },
    {
      "cells": [
        "467321101899",
        "9.75"
      ],
      "line": 11
    },
    {
      "cells": [
        "477321101878",
        "17.25"
      ],
      "line": 12
    }
  ],
  "keyword": "Given "
});
formatter.step({
  "line": 14,
  "name": "User Navigates to create Shipment Page",
  "keyword": "When "
});
formatter.step({
  "line": 16,
  "name": "price from the page should match the given upc",
  "keyword": "Then "
});
formatter.match({
  "location": "PriceConfirmStepDefinition.the_following_Data(DataTable)"
});
formatter.result({
  "duration": 2944787,
  "status": "passed"
});
formatter.match({
  "location": "PriceConfirmStepDefinition.user_Navigates_to_create_Shipment_Page()"
});
formatter.result({
  "duration": 6539463227,
  "status": "passed"
});
formatter.match({
  "location": "PriceConfirmStepDefinition.price_from_the_page_should_match_the_given_upc()"
});
formatter.result({
  "duration": 653023393,
  "status": "passed"
});
formatter.after({
  "duration": 694668994,
  "status": "passed"
});
formatter.after({
  "duration": 754049808,
  "status": "passed"
});
});